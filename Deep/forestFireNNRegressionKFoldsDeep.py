import os
import sys
from sklearn import cross_validation
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.noise import GaussianNoise
from keras.optimizers import SGD
from keras.layers.advanced_activations import PReLU
import numpy as np
import math
import random
from multiprocessing import Process, Pool
import thread
import theano
import time


#mean of output is: 12.8472920696
#std deviation is: 63.5942259828

# i0 mean: 4.669246  std: 2.311539
# i1 mean: 4.299807  std: 1.228710
# i21 mean: 90.644684  std: 5.514770
# i22 mean: 110.872337  std: 63.984512
# i23 mean: 547.940063  std: 247.826172
# i24 mean: 9.021664  std: 4.555066
# i25 mean: 18.889168  std: 5.801007
# i26 mean: 44.288200  std: 16.301682
# i27 mean: 4.017601  std: 1.789919
# i28 mean: 0.021663  std: 0.295673

def doRegression(threadName,delay):
    print threadName
    sys.stdout.flush()
    inputNodes = 29
    layer1Activations = ['relu','tanh','sigmoid','hard_sigmoid','linear']
    layer2Activations = ['relu','tanh','sigmoid','hard_sigmoid','linear']
    hiddenLayer1Nodes = 10
    hiddenLayer2Nodes = 0
    hiddenLayer3Nodes = 0
    outputLayerNodes = 1
    random.seed
    inFile = open('forestfires.txt', 'r')
    if(threadName=="Thread-1"):
     outFile = open('resultDataRelu.txt','a')
    if(threadName=="Thread-2"):
     outFile = open('resultDataTanh.txt','a')
    if(threadName=="Thread-3"):
     outFile = open('resultDataSigmoid.txt','a')
    if(threadName=="Thread-4"):
     outFile = open('resultDataHardSigmoid.txt','a')
    outFile.flush()
    completeInputData  = []
    completeOutputData = []
    allRealOutputs     = []

    for line in inFile:
        inputData = line.split()
        output = float(inputData[-1].strip())
        inputData = map(float,inputData)

        #0-1 normalization from max values of field
        inputData[0]  = float(inputData[0] / 9)
        inputData[1]  = float(inputData[1] / 9)
        inputData[21] = float(inputData[21] / 96.20)
        inputData[22] = float(inputData[22] / 291.3)
        inputData[23] = float(inputData[23] / 860.6)
        inputData[24] = float(inputData[24] / 56.10)
        inputData[25] = float(inputData[25] / 33.30)
        inputData[26] = float(inputData[26] / 100)
        inputData[27] = float(inputData[27] / 9.40)
        inputData[28] = float(inputData[28] / 6.4)

        allRealOutputs.append(output)
        completeOutputData.append((np.log(1+output))/6.994)
        completeInputData.append(inputData[0:29])

    X = np.array(completeInputData,  "float32")
    Y = np.array(completeOutputData, "float32")

    totalRounds = 1
    if threadName == "Thread-1":
        firstActivation = layer1Activations[0]
    if threadName == "Thread-2":
        firstActivation = layer1Activations[1]
    if threadName == "Thread-3":
        firstActivation = layer1Activations[2]
    if threadName == "Thread-4":
        firstActivation = layer1Activations[3]
    if threadName == "Thread-5":
        firstActivation = layer1Activations[4]

    for j in range(0,len(layer2Activations)):
        for numNodes in range(0, 50):
            numNodes += 1
            totalRMSE = 0
            for round in range(0,totalRounds):
                start_time = time.time()
                kf = cross_validation.KFold(n=len(completeInputData),n_folds=5,shuffle=True)
                totalPasses = 0
                foldRMSE = 0

                for train_index,test_index in kf:
                    totalPasses += 1
                    X_train, X_test = X[train_index], X[test_index]
                    Y_train, Y_test = Y[train_index], Y[test_index]

                    #####Start NN Section ######
                    model = Sequential()
                    sgd = SGD(lr=0.05,momentum=0.05)
                    model.add(Dense(inputNodes, numNodes,init='normal'))
                    model.add(Activation(firstActivation))
                    model.add(Dense(numNodes, outputLayerNodes,init='normal'))
                    model.add(Activation(layer2Activations[j]))
                    model.compile(loss='mse', optimizer=sgd)
                    model.fit(X_train, Y_train, nb_epoch=300,shuffle=1,verbose=0,batch_size=8)

                    predictions = (model.predict(X_test))

                    #reverse normalization
                    for i in range (0,len(predictions)):
                        predictions[i] *= 6.994
                        predictions[i] = math.exp(predictions[i])-1

                        Y_test[i] *= 6.994
                        Y_test[i] = math.exp(Y_test[i])-1

                        #print predictions[i]

                    mse = np.mean(np.square(predictions - Y_test))

                    score = np.sqrt(mse)
                    foldRMSE += score
                    # print "RMSE:"
                    # print score
                    # for i in range (0,len(predictions)):
                    #     print "%f           %f          "%(predictions[i],Y_test[i])

                # print "\nFolds: %i" % totalPasses
                # print "RMSE: %f\n\n" % float(foldRMSE/totalPasses)
                totalRMSE += float(foldRMSE/totalPasses)

            # print "\n Rounds: %i" % totalRounds
            print("RMSE: %f , inputNodes: %i, Layer 1 Act: %s,HL1 Nodes: %i, Layer 2 Act: %s, Time: %f\n" % (float(totalRMSE/totalRounds),inputNodes,firstActivation,numNodes,layer2Activations[j],time.time()-start_time))
            outFile.write("%f,%i,%s,%i,%s,%f\n" % (float(totalRMSE/totalRounds),inputNodes,firstActivation,numNodes,layer2Activations[j], time.time()-start_time))
    outFile.close()

def testFunction():
    print ""

if __name__=='__main__':
    # p1 = Process(target=doRegression("Thread-1",4))
    # p1.start()
    # p2 = Process(target=doRegression("Thread-2",6))
    # p2.start()
    # p3 = Process(target=doRegression("Thread-3",8))
    # p3.start()
    # p4 = Process(target=doRegression("Thread-4",10))
    # p4.start()
    # p5 = Process(target=doRegression("Thread-5",12))
    # p5.start()

    pool = Pool()
    pool.apply_async(doRegression,args=("Thread-1",4))
    pool.apply_async(doRegression,args=("Thread-2",4))
    pool.apply_async(doRegression,args=("Thread-3",4))
    pool.apply_async(doRegression,args=("Thread-4",4))

    pool.close()
    pool.join()

import os
from sklearn import cross_validation
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.noise import GaussianNoise
from keras.optimizers import SGD
import numpy as np
import math
import random
import theano.tensor as T
import time

class Particle:
    def __init__(self,position,error,velocity,bestPosition,bestError):
        self.position = position
        self.error = error
        self.velocity = velocity
        self.bestPosition = bestPosition
        self.bestError = bestError

class NeuralNetwork:
    def __init__(self,numInputNodes,numHiddenNodes,numOutputNodes):
        self.numInputNodes = numInputNodes
        self.numHiddenNodes = numHiddenNodes
        self.numOutNodes = numOutputNodes
        self.inputs = np.zeros(shape=numInputNodes)
        self.ihWeights = self.MakeMatrix(numInputNodes,numHiddenNodes)
        self.hBiases = np.zeros(shape=self.numHiddenNodes)
        self.hOutputs = np.zeros(shape=numHiddenNodes)
        self.hoWeights = self.MakeMatrix(numHiddenNodes,numOutputNodes)
        self.oBiases = np.zeros(shape=self.numOutNodes)
        self.outputs = np.zeros(shape=numOutputNodes)

    def MakeMatrix(self,rows,columns):
        return np.zeros(shape=(rows,columns))

    def SetWeights(self,weights):
        #copy weights and biases in weights[] array to i-h weights, i-h biases, h-o weights, h-o biases
        numWeights = (self.numInputNodes * self.numHiddenNodes) + (self.numHiddenNodes * self.numOutNodes) + self.numHiddenNodes + self.numOutNodes

        k = 0

        for i in range(0,self.numInputNodes):
            for j in range(0,self.numHiddenNodes):
                self.ihWeights[i][j] = weights[k]
                k += 1
        for i in range(0,self.numHiddenNodes):
            self.hBiases[i] = weights[k]
            k += 1
        for i in range(0,self.numHiddenNodes):
            for j in range(0,self.numOutNodes):
                self.hoWeights[i][j] = weights[k]
                k += 1
        for i in range(0,self.numOutNodes):
            self.oBiases[i] = weights[k]
            k += 1

    def GetWeights(self):
        #returns the current set of weights, presumably after training
        numWeights = (self.numInputNodes * self.numHiddenNodes) + (self.numHiddenNodes * self.numOutNodes) + self.numHiddenNodes + self.numOutNodes

        k = 0
        result = np.empty(shape=(numWeights))
        for i in range(0,self.ihWeighth.size):
            for j in range(0,self.ihWeights[0].size):
                result[k] = self.ihWeights[i][j]
                k += 1
        for i in range(0,self.hBiases.size):
            result[k] = self.hBiases[i]
            k += 1
        for i in range(0,self.hoWeights.size):
            for j in range(0,self.hoWeights[0].size):
                result[k] = self.hoWeights[i][j]
                k += 1
        for i in range(0,self.oBiases.size):
            result[k] = self.oBiases[i]
            k += 1
        return result

    def ComputeOutputs(self,xValues):
        hSums = np.zeros(shape=(self.numHiddenNodes))
        oSums = np.empty(shape=(self.numOutNodes))

        for i in range(0,xValues.size):
            self.inputs[i] = xValues[i]

        for j in range(0,self.numHiddenNodes):
            for i in range(0,self.numInputNodes):
                hSums[j] += self.inputs[i] * self.ihWeights[i][j]

        for i in range(0,self.numHiddenNodes):
            hSums[i] += self.hBiases[i]

        for i in range(0,self.numHiddenNodes):
            self.hOutputs[i] = self.Sigmoid(hSums[i])

        for j in range(0,self.numOutNodes):
            for i in range(0,self.numHiddenNodes):
                oSums[j] += self.hOutputs[i] * self.hoWeights[i][j]

        for i in range(0,self.numOutNodes):
            oSums[i] += self.oBiases[i]

        hyperTanOut = self.Sigmoid(oSums[0])
        self.outputs = hyperTanOut


        retResult = self.outputs

        return retResult

    def Sigmoid(self,x):
        return 1/(1 + math.exp(-x))

    def HardSigmoid(self,x):
        return T.nnet.hard_sigmoid(x)

    def HyperTanFunction(self,x):
        return math.tanh(x)

    def Softplus(self,x):
        return math.log10(1 + math.exp(x))

    def Softmax(self,oSums):
        max = oSums[0]
        for i in range(0,oSums.size):
            if(oSums[i] > max):
                max = oSums[i]

        scale = 0.0
        for i in range(0,oSums.size):
            scale += math.exp(oSums[i] - max)

        result = np.empty(shape=(oSums.size))
        for i in range(0,oSums.size):
            result[i] = math.exp(oSums[i] - max) / scale

        return result

    def Train(self,trainData,numParticles,maxEpochs,exitError,probDeath):
        #PSO version training. best weights stored into NN and returned
        #particle posistion == NN weights

        numWeights = (self.numInputNodes * self.numHiddenNodes) + (self.numHiddenNodes * self.numOutNodes) + self.numHiddenNodes + self.numOutNodes

        #use PSO to seek best weights
        epoch = 0
        minX  = -1.0
        maxX  = 1.0
        w     = 0.729   #inertia weight
        c1    = 1.49445    #cognitive/local weight
        c2    = 1.49445    #social/global weight

        swarm = np.empty(shape=(numParticles,),dtype=object)
        for i in range(0,numParticles):
            swarm[i] = Particle(0.0, 9999.0, 0.0, 0.0, 9999.0)

        bestGlobalPosition = np.zeros(shape=numWeights)
        bestGlobalError = 99999999.0
        prevGlobalError = 0

        random.seed
        #init each Particle in the swarm with random positions and velocities
        for i in range(0, swarm.size):
            randomPosition = np.zeros(shape = numWeights)
            for j in range(0, randomPosition.size):
                randomPosition[j] = (maxX - minX) * random.random() + minX

            error = self.RootMeanSquaredError(trainData, randomPosition)
            randomVelocity = np.zeros(shape=numWeights)

            for j in range(0, randomVelocity.size):
                lo = 0.05 * minX
                hi = 0.05 * maxX
                randomVelocity[j] = (hi - lo) * random.random() + lo

            swarm[i] = Particle(randomPosition, error, randomVelocity, randomPosition, error)

            #does current Particle have global best posistion/solution?
            if swarm[i].error < bestGlobalError:
                bestGlobalError    = swarm[i].error
                bestGlobalPosition = swarm[i].position
                prevGlobalError = bestGlobalError



        # main PSO algorithm
        sequence = np.zeros(shape=numParticles)
        for i in range(0,sequence.size):
            sequence[i] = i

        while epoch < maxEpochs:
            globalUpdated = 0
            print bestGlobalError
            if bestGlobalError < exitError:
                break

            random.shuffle(sequence) #move particles in random sequence
            
            for pi in range(0, swarm.size): #each particle (index)
                newVelocity = np.zeros(shape=numWeights)
                newPosition = np.zeros(shape=numWeights)

                i = sequence[pi]
                currP = swarm[i]
                
                #compute new velocity
                for j in range(0, currP.velocity.size):
                    r1 = random.random()
                    r2 = random.random()
                    #velocity depeonds on old velocity, best posistion of particle, and best position of any particle
                    newVelocity[j] = (w * currP.velocity[j]) + (c1 * r1 * (currP.bestPosition[j] - currP.position[j])) + (c2 * r2 * (bestGlobalPosition[j] - currP.position[j]))

                currP.velocity = newVelocity
                
                #use new velocity to compute new position
                for j in range(0, currP.position.size):
                    newPosition[j] = currP.position[j] + newVelocity[j]
                    if newPosition[j] < minX:
                        newPosition[j] = minX
                    if newPosition[j] > maxX:
                        newPosition[j] = maxX

                currP.position = newPosition



                #use new position to compute new error
                newError = self.RootMeanSquaredError(trainData, newPosition)
                currP.error = newError

                if newError < currP.bestError:
                    currP.bestPosition = newPosition
                    currP.bestError = newError

                if newError < bestGlobalError:
                    globalUpdated = 1
                    bestGlobalPosition = newPosition
                    bestGlobalError = newError

                #does particle die?
                die = random.random()
                if(die < probDeath):
                    for j in range(0, currP.position.size):
                        currP.position[j] = (maxX - minX) + random.random() + minX

                    currP.error = self.RootMeanSquaredError(trainData, currP.position)
                    currP.bestPosition = currP.position
                    currP.bestError = currP.error

                    if currP.error < bestGlobalError:
                        bestGlobalError = currP.error
                        bestGlobalPosition = currP.position

            if(globalUpdated==0):
                bestGlobalError = 99999.0
                #init each Particle in the swarm with random positions and velocities
                for i in range(0, swarm.size):
                    randomPosition = np.zeros(shape = numWeights)
                    for j in range(0, randomPosition.size):
                        randomPosition[j] = (maxX - minX) * random.random() + minX

                    error = self.RootMeanSquaredError(trainData, randomPosition)
                    randomVelocity = np.zeros(shape=numWeights)

                    for j in range(0, randomVelocity.size):
                        lo = 0.05 * minX
                        hi = 0.05 * maxX
                        randomVelocity[j] = (hi - lo) * random.random() + lo

                    swarm[i] = Particle(randomPosition, error, randomVelocity, randomPosition, error)

                    #does current Particle have global best posistion/solution?
                    if swarm[i].error < bestGlobalError:
                        bestGlobalError    = swarm[i].error
                        bestGlobalPosition = swarm[i].position


            epoch += 1

        self.SetWeights(bestGlobalPosition)
        retResult = bestGlobalPosition
        return retResult

    def RootMeanSquaredError(self, trainData, weights):
        self.SetWeights(weights)

        sumSquaredError = 0.0

        for i in range(0,trainData.shape[0]):
            xValues = trainData[i][0:29]
            tValues = trainData[i][-1]
            tValues *= 6.994
            tValues = math.exp(tValues)-1
            yValues = self.ComputeOutputs(xValues)
            yValues *= 6.994
            yValues = math.exp(yValues)-1

            sumSquaredError += ((yValues - tValues) * (yValues - tValues))

        return math.sqrt(sumSquaredError / trainData.shape[0])


    def Accuracy(self,testData):
        numCorrect = 0
        numWrong = 0

        for i in range(0,testData.shape[0]):
            xValues = testData[i][0:4]
            tValues = testData[i][4:]
            yValues = self.ComputeOutputs(xValues)
            maxIndex = np.argmax(yValues)

            if tValues[maxIndex] == 1.0:
                numCorrect += 1
            else:
                numWrong += 1

        return (numCorrect * 1.0) / (numCorrect + numWrong)

def doRegression():

    outFile = open('resultsPSO2.txt','a')
    trainData = np.zeros(shape=(24,7))
    testData = np.zeros(shape=(6,7))

    inFile = open('forestfires.txt', 'r')

    completeData  = []
    allRealOutputs     = []

    for line in inFile:
        inputData = line.split()
        output = float(inputData[-1].strip())
        inputData = map(float,inputData)

        #0-1 normalization from max values of field
        inputData[0]  = float(inputData[0] / 9)
        inputData[1]  = float(inputData[1] / 9)
        inputData[21] = float(inputData[21] / 96.20)
        inputData[22] = float(inputData[22] / 291.3)
        inputData[23] = float(inputData[23] / 860.6)
        inputData[24] = float(inputData[24] / 56.10)
        inputData[25] = float(inputData[25] / 33.30)
        inputData[26] = float(inputData[26] / 100)
        inputData[27] = float(inputData[27] / 9.40)
        inputData[28] = float(inputData[28] / 6.4)
        inputData[-1] = np.log(1+output)/6.994

        allRealOutputs.append(output)
        completeData.append(inputData)

    X = np.array(completeData,  "float32")

    totalRounds = 1
    kFolds = 5
    for round in range(0,totalRounds):
        kf = cross_validation.KFold(n=len(completeData),n_folds=kFolds,shuffle=True)
        totalPasses = 0
        foldRMSE = 0
        for numHiddenNodes in range(5,150):
            totalRMSE = 0
            for train_index,test_index in kf:
                start_time = time.time()
                totalPasses += 1
                X_train, X_test = X[train_index], X[test_index]

                # print "Training Data:"
                # ShowMatrix(X_train,X_train.shape[0],1,True)
                #
                # print "Testing Data:"
                # ShowMatrix(X_test,X_test.shape[0],1,True)

                numInputNodes = 29
                # numHiddenNodes = 5
                numOutputNodes = 1

                nn = NeuralNetwork(numInputNodes,numHiddenNodes,numOutputNodes)

                numParticles = 250
                maxEpochs = 50
                exitError = 17
                probDeath = 0.005

                bestWeights = nn.Train(X_train, numParticles, maxEpochs, exitError, probDeath)

                # print "\nbest Weights:"
                # ShowVector(bestWeights,10,3,True)

                nn.SetWeights(bestWeights)

                RMSE = nn.RootMeanSquaredError(X_test,bestWeights)
                totalRMSE += RMSE
                #print "\n"
                print "round RMSE:"
                print RMSE
                #print "\n"

            print "K-Fold RMSE:"
            print totalRMSE/kFolds
            print "\nRMSE: %f, Particles: %i, Nodes: %i,Time: %f\n" % ((totalRMSE/kFolds), numParticles, numHiddenNodes, ((time.time()-start_time)/kFolds))
            outFile.write("%f,%i,%i,%f\n" % ((totalRMSE/kFolds),numParticles,numHiddenNodes,((time.time()-start_time)/kFolds)))

    # trainAcc = nn.Accuracy(trainData)
    # print ("Accuracy on Training: %f" % trainAcc)
    #
    # testAcc = nn.Accuracy(testData)
    # print("Accuracy on testing: %f" % testAcc)


def ShowMatrix(matrix,numRows,decimals,newLine):
    for i in range(0,numRows):
        for j in range(0,matrix[i].size):
            if matrix[i][j] >= 0.0:
                print " ",
            else:
                print "-",
            print ("%f" % matrix[i][j]),
        print "",
        if newLine == True:
            print ""

def ShowVector(vector,valsPerRow,decimals,newLine):
    for i in range(0,vector.shape[0]):
       if(i % valsPerRow == 0):
           print ""
       else:
           print ("%f" % vector[i]),
    if(newLine == True):
        print ""


if __name__=='__main__':
    doRegression()
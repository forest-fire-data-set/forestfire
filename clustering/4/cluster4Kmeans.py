from sklearn.feature_selection import VarianceThreshold, SelectKBest,chi2,f_regression,f_classif
from sklearn.decomposition import PCA
import numpy as np
from sklearn.cluster import KMeans,AffinityPropagation,SpectralClustering
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
import csv
import  math
import os

fires29 = pandas.read_csv(os.path.dirname(__file__)+"/../forestfires29.txt")

affinity_model = AffinityPropagation(damping=.5,max_iter=30000,convergence_iter=1000,verbose=1)
spectral_model = SpectralClustering(n_clusters=4)
kmeans_model = KMeans(n_clusters=4, random_state=2, verbose=1)

fires29All = fires29._get_numeric_data()
cols_to_norm = ['X','Y','FFMC','DMC','DC','ISI','Temp','RH','Wind','Rain']
fires29All[cols_to_norm] = fires29All[cols_to_norm].apply(lambda x: (x - x.mean()) / (x.max() - x.min()))

cols_to_norm = ['Area']
fires29All[cols_to_norm] = fires29All[cols_to_norm].apply(lambda x: np.log10(x + 1, dtype='float64'))

fires29Inputs = fires29All[[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]]
fires29Outputs = fires29All[[29]]

matrixOfAllFires29 = fires29All.as_matrix()
matrixOfInputs29 = fires29Inputs.as_matrix()
matrixOfOutputs29 = fires29Outputs.as_matrix()

kmeans_model.fit(matrixOfOutputs29)
labels = kmeans_model.labels_

# affinity_model.fit(matrixOfAllFires)
# labels = affinity_model.labels_
#
# spectral_model.fit(matrixOfAllFires)
# labels = spectral_model.labels_

print labels

with open(os.path.dirname(__file__)+"/../forestfires29.txt") as csvinput:
    with open("4KMeans29Outputs.txt",'w') as csvoutput:
        reader = csv.reader(csvinput)
        writer = csv.writer(csvoutput,lineterminator='\n')
        all = []
        row = next(reader)
        row.append('Class')
        all.append(row)
        for i in range(0,517):
            row = next(reader)
            row.append(labels[i])
            all.append(row)
        writer.writerows(all)

pca_2 = PCA(2)
plot_columns = pca_2.fit_transform(matrixOfAllFires29)
plt.scatter(x=plot_columns[:,0], y=plot_columns[:,1],c=labels,cmap='rainbow')
plt.show()


pca_3 = PCA(3)
plot_columns = pca_3.fit_transform(matrixOfAllFires29)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(plot_columns[:, 0], plot_columns[:, 1], plot_columns[:, 2], c=labels, marker='o',cmap='rainbow')
plt.show()

##################################################################################################
fires12 = pandas.read_csv(os.path.dirname(__file__)+"/../forestfires12.txt")

affinity_model = AffinityPropagation(damping=.5,max_iter=30000,convergence_iter=1000,verbose=1)
spectral_model = SpectralClustering(n_clusters=4)
kmeans_model = KMeans(n_clusters=4, random_state=2, verbose=1)

fires12All = fires12._get_numeric_data()
cols_to_norm = ['X','Y','FFMC','DMC','DC','ISI','Temp','RH','Wind','Rain']
fires12All[cols_to_norm] = fires12All[cols_to_norm].apply(lambda x: (x - x.mean()) / (x.max() - x.min()))

cols_to_norm = ['Area']
fires12All[cols_to_norm] = fires12All[cols_to_norm].apply(lambda x: np.log10(x + 1, dtype='float64'))

fires12Inputs = fires12All[[0,1,2,3,4,5,6,7,8,9,10,11]]
fires12Outputs = fires12All[[12]]

matrixOfAllFires12 = fires12All.as_matrix()
matrixOfInputs12 = fires12Inputs.as_matrix()
matrixOfOutputs12 = fires12Outputs.as_matrix()

kmeans_model.fit(matrixOfOutputs12)
labels = kmeans_model.labels_

# affinity_model.fit(matrixOfAllFires)
# labels = affinity_model.labels_
#
# spectral_model.fit(matrixOfAllFires)
# labels = spectral_model.labels_

print labels

with open(os.path.dirname(__file__)+"/../forestfires12.txt") as csvinput:
    with open("4KMeans12Outputs.txt",'w') as csvoutput:
        reader = csv.reader(csvinput)
        writer = csv.writer(csvoutput,lineterminator='\n')
        all = []
        row = next(reader)
        row.append('Class')
        all.append(row)
        for i in range(0,517):
            row = next(reader)
            row.append(labels[i])
            all.append(row)
        writer.writerows(all)

pca_2 = PCA(2)
plot_columns = pca_2.fit_transform(matrixOfAllFires12)
plt.scatter(x=plot_columns[:,0], y=plot_columns[:,1],c=labels,cmap='rainbow')
plt.show()


pca_3 = PCA(3)
plot_columns = pca_3.fit_transform(matrixOfAllFires12)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(plot_columns[:, 0], plot_columns[:, 1], plot_columns[:, 2], c=labels, marker='o',cmap='rainbow')
plt.show()

####################################################################################################
#############################################################
fires29 = pandas.read_csv(os.path.dirname(__file__)+"/../forestfires29.txt")

affinity_model = AffinityPropagation(damping=.5,max_iter=30000,convergence_iter=1000,verbose=1)
spectral_model = SpectralClustering(n_clusters=4)
kmeans_model = KMeans(n_clusters=4, random_state=2, verbose=1)

fires29All = fires29._get_numeric_data()
cols_to_norm = ['X','Y','FFMC','DMC','DC','ISI','Temp','RH','Wind','Rain']
fires29All[cols_to_norm] = fires29All[cols_to_norm].apply(lambda x: (x - x.mean()) / (x.max() - x.min()))

cols_to_norm = ['Area']
fires29All[cols_to_norm] = fires29All[cols_to_norm].apply(lambda x: np.log10(x + 1, dtype='float64'))

fires29Inputs = fires29All[[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]]
fires29Outputs = fires29All[[29]]

matrixOfAllFires29 = fires29All.as_matrix()
matrixOfInputs29 = fires29Inputs.as_matrix()
matrixOfOutputs29 = fires29Outputs.as_matrix()

kmeans_model.fit(matrixOfInputs29)
labels = kmeans_model.labels_

# affinity_model.fit(matrixOfAllFires)
# labels = affinity_model.labels_
#
# spectral_model.fit(matrixOfAllFires)
# labels = spectral_model.labels_

print labels

with open(os.path.dirname(__file__)+"/../forestfires29.txt") as csvinput:
    with open("4KMeans29Inputs.txt",'w') as csvoutput:
        reader = csv.reader(csvinput)
        writer = csv.writer(csvoutput,lineterminator='\n')
        all = []
        row = next(reader)
        row.append('Class')
        all.append(row)
        for i in range(0,517):
            row = next(reader)
            row.append(labels[i])
            all.append(row)
        writer.writerows(all)

pca_2 = PCA(2)
plot_columns = pca_2.fit_transform(matrixOfAllFires29)
plt.scatter(x=plot_columns[:,0], y=plot_columns[:,1],c=labels,cmap='rainbow')
plt.show()


pca_3 = PCA(3)
plot_columns = pca_3.fit_transform(matrixOfAllFires29)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(plot_columns[:, 0], plot_columns[:, 1], plot_columns[:, 2], c=labels, marker='o',cmap='rainbow')
plt.show()

##################################################################################################
fires12 = pandas.read_csv(os.path.dirname(__file__)+"/../forestfires12.txt")

affinity_model = AffinityPropagation(damping=.5,max_iter=30000,convergence_iter=1000,verbose=1)
spectral_model = SpectralClustering(n_clusters=4)
kmeans_model = KMeans(n_clusters=4, random_state=2, verbose=1)

fires12All = fires12._get_numeric_data()
cols_to_norm = ['X','Y','FFMC','DMC','DC','ISI','Temp','RH','Wind','Rain']
fires12All[cols_to_norm] = fires12All[cols_to_norm].apply(lambda x: (x - x.mean()) / (x.max() - x.min()))

cols_to_norm = ['Area']
fires12All[cols_to_norm] = fires12All[cols_to_norm].apply(lambda x: np.log10(x + 1, dtype='float64'))

fires12Inputs = fires12All[[0,1,2,3,4,5,6,7,8,9,10,11]]
fires12Outputs = fires12All[[12]]

matrixOfAllFires12 = fires12All.as_matrix()
matrixOfInputs12 = fires12Inputs.as_matrix()
matrixOfOutputs12 = fires12Outputs.as_matrix()

kmeans_model.fit(matrixOfInputs12)
labels = kmeans_model.labels_

# affinity_model.fit(matrixOfAllFires)
# labels = affinity_model.labels_
#
# spectral_model.fit(matrixOfAllFires)
# labels = spectral_model.labels_

print labels

with open(os.path.dirname(__file__)+"/../forestfires12.txt") as csvinput:
    with open("4KMeans12Inputs.txt",'w') as csvoutput:
        reader = csv.reader(csvinput)
        writer = csv.writer(csvoutput,lineterminator='\n')
        all = []
        row = next(reader)
        row.append('Class')
        all.append(row)
        for i in range(0,517):
            row = next(reader)
            row.append(labels[i])
            all.append(row)
        writer.writerows(all)

pca_2 = PCA(2)
plot_columns = pca_2.fit_transform(matrixOfAllFires12)
plt.scatter(x=plot_columns[:,0], y=plot_columns[:,1],c=labels,cmap='rainbow')
plt.show()


pca_3 = PCA(3)
plot_columns = pca_3.fit_transform(matrixOfAllFires12)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(plot_columns[:, 0], plot_columns[:, 1], plot_columns[:, 2], c=labels, marker='o',cmap='rainbow')
plt.show()

####################################################################################################
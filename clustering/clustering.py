from sklearn.feature_selection import VarianceThreshold, SelectKBest,chi2,f_regression,f_classif
from sklearn.decomposition import PCA
import numpy as np
from sklearn.cluster import KMeans,AffinityPropagation,SpectralClustering
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor
import csv
import  math

fires = pandas.read_csv("forestfires29.txt")

affinity_model = AffinityPropagation(damping=.5,max_iter=30000,convergence_iter=1000,verbose=1)
spectral_model = SpectralClustering(n_clusters=3)
kmeans_model = KMeans(n_clusters=3, random_state=2, verbose=1)

firesAll = fires._get_numeric_data()
cols_to_norm = ['X','Y','FFMC','DMC','DC','ISI','Temp','RH','Wind','Rain']
firesAll[cols_to_norm] = firesAll[cols_to_norm].apply(lambda x: (x - x.mean()) / (x.max() - x.min()))

cols_to_norm = ['Area']
firesAll[cols_to_norm] = firesAll[cols_to_norm].apply(lambda x: np.log10(x + 1, dtype='float64'))

firesInputs = firesAll[[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28]]
firesOutputs = firesAll[[29]]

matrixOfAllFires = firesAll.as_matrix()
matrixOfInputs = firesInputs.as_matrix()
matrixOfOutputs = firesOutputs.as_matrix()

kmeans_model.fit(matrixOfAllFires)
labels = kmeans_model.labels_
#
#
# print labels
# print "CENTROIDS"
# print kmeans_model.cluster_centers_
#
# affinity_model.fit(matrixOfAllFires)
# labels = affinity_model.labels_
#
# spectral_model.fit(matrixOfAllFires)
# labels = spectral_model.labels_

print labels

with open("forestfires29.txt") as csvinput:
    with open("3KMeansAll.txt",'w') as csvoutput:
        reader = csv.reader(csvinput)
        writer = csv.writer(csvoutput,lineterminator='\n')

        all = []
        row = next(reader)
        row.append('Class')
        all.append(row)
        for i in range(0,517):
            row = next(reader)
            row.append(labels[i])
            all.append(row)
        writer.writerows(all)

pca_2 = PCA(2)
plot_columns = pca_2.fit_transform(matrixOfAllFires)
plt.scatter(x=plot_columns[:,0], y=plot_columns[:,1],c=labels)
plt.show()


# pca_3 = PCA(3)
# plot_columns = pca_3.fit_transform(good_columns)
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(plot_columns[:, 0], plot_columns[:, 1], plot_columns[:, 2], c=labels, marker='o')
# plt.show()

# columns = fires.columns.tolist()
# columns = [c for c in columns if c not in ["ISI","Rain"]]
#
# target = "Area"
#
# train = fires.sample(frac = 0.8, random_state=2)
# test = fires.loc[~fires.index.isin(train.index)]
#
# model = RandomForestRegressor(n_estimators=100,min_samples_leaf=10,random_state=2)
# model.fit(train[columns],train[target])
#
# predictions = model.predict(test[columns])
#
# print mean_squared_error(predictions,test[target])
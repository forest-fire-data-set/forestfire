from keras.models import Sequential
from keras.layers.core import Dense,Activation,Dropout
from keras.optimizers import SGD,RMSprop,Adagrad,Adam,Adadelta
import numpy as np
from sklearn import cross_validation
import math
from multiprocessing import Process, Pool



def doRegression(threadName,delay):
    print threadName
    #sys.stdout.flush()

    outputNodes = 3
    inFile = open('3KMeans29Outputs.txt', 'rU')
    layer1Activations = ['relu','tanh','sigmoid','hard_sigmoid','linear']

    if(threadName=="Thread-1"):
        outFile = open('29resultDataReluClassifierOutputs.txt','a')
    if(threadName=="Thread-2"):
        outFile = open('29resultDataTanhClassifierOutputs.txt','a')
    if(threadName=="Thread-3"):
        outFile = open('29resultDataSigmoidClassifierOutputs.txt','a')
    if(threadName=="Thread-4"):
        outFile = open('29resultDataHardSigmoidClassifierOutputs.txt','a')
    if(threadName=="Thread-5"):
        outFile = open('29resultDataLinearClassifierOutputs.txt','a')
    outFile.flush()

    completeData   = []
    allRealOutputs = []
    sampleData = []
    targetData = []
    with inFile as f:
        next(f)
        for line in inFile:
            inputData = line.split(',')
            output = float(inputData[-1].strip())
            inputData = map(float,inputData)

            #0-1 normalization from max values of field
            inputData[0]  = float(inputData[0] / 9)
            inputData[1]  = float(inputData[1] / 9)
            inputData[21] = float(inputData[21] / 96.20)
            inputData[22] = float(inputData[22] / 291.3)
            inputData[23] = float(inputData[23] / 860.6)
            inputData[24] = float(inputData[24] / 56.10)
            inputData[25] = float(inputData[25] / 33.30)
            inputData[26] = float(inputData[26] / 100)
            inputData[27] = float(inputData[27] / 9.40)
            inputData[28] = float(inputData[28] / 6.4)
            #inputData[-1] = output

            if output == 0:
                burnClass = [1,0,0]
            if output == 1:
                burnClass = [0,1,0]
            if output == 2:
                burnClass = [0,0,1]

            targetData.append((burnClass[0],burnClass[1],burnClass[2]))
            allRealOutputs.append(output)
            sampleData.append(inputData[0:29])

    X = np.array(sampleData,  "float32")
    Y = np.array(targetData, "float32")
    Z = np.array(allRealOutputs, "float32")

    if threadName == "Thread-1":
        firstActivation = layer1Activations[0]
    if threadName == "Thread-2":
        firstActivation = layer1Activations[1]
    if threadName == "Thread-3":
        firstActivation = layer1Activations[2]
    if threadName == "Thread-4":
        firstActivation = layer1Activations[3]
    if threadName == "Thread-5":
        firstActivation = layer1Activations[4]

    kFolds = 5
    kf = cross_validation.KFold(n=len(sampleData),n_folds=kFolds,shuffle=True)
    skf = cross_validation.StratifiedKFold(Z,n_folds=5,shuffle=True)

    for numNodes in range(1,200):
        numNodes +=1
        totalScore = 0
        totalAccuracy = 0
        for train_index,test_index in skf:
            X_train, X_test = X[train_index], X[test_index]
            Y_train, Y_test = Y[train_index], Y[test_index]
            model = Sequential()
            model.add(Dense(29,numNodes,init='uniform'))
            model.add(Activation('tanh'))
            model.add(Dense(numNodes,outputNodes, init='uniform'))
            model.add(Activation('softmax'))

            sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
            rmsProp = RMSprop(lr=0.001, rho=0.9, epsilon=1e-06)
            adagrad = Adagrad(lr=0.01, epsilon=1e-06)
            adadelta = Adadelta(lr=1.0,rho=0.95,epsilon=1e-06)
            adam = Adam(lr=0.001,beta_1=0.9,beta_2=0.999,epsilon=1e-08)

            model.compile(loss='mse', optimizer=rmsProp)

            model.fit(X_train, Y_train,nb_epoch=300, batch_size=128, show_accuracy=True, verbose=0)
            score = model.evaluate(X_test, Y_test, batch_size=128, show_accuracy=True, verbose=0)

            totalScore += math.sqrt(score[0])
            totalAccuracy += score[1]
        outFile.write("%f,%f,%i,%s \n" % (totalScore/kFolds,totalAccuracy/kFolds,numNodes,firstActivation))
        print totalScore/kFolds,totalAccuracy/kFolds,numNodes,firstActivation
        # print "3 Clusters Score: ", totalScore/kFolds
        # print "3 Clusters Accuracy: ", totalAccuracy/kFolds

if __name__=='__main__':
    pool = Pool()
    # pool.apply_async(doRegression,args=("Thread-1",4))
    # pool.apply_async(doRegression,args=("Thread-2",4))
    # pool.apply_async(doRegression,args=("Thread-3",4))
    # pool.apply_async(doRegression,args=("Thread-4",4))
    # pool.apply_async(doRegression,args=("Thread-5",5))

    pool.close()
    pool.join()


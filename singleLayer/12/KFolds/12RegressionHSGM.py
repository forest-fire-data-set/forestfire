import os
import sys
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedKFold
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.layers.noise import GaussianNoise
from keras.optimizers import SGD
from keras.layers.advanced_activations import PReLU
import numpy as np
import math
import random
from multiprocessing import Process, Pool
import thread
import theano
import time
import os.path


#mean of output is: 12.8472920696
#std deviation is: 63.5942259828

# i0 mean: 4.669246  std: 2.311539
# i1 mean: 4.299807  std: 1.228710
# i21 mean: 90.644684  std: 5.514770
# i22 mean: 110.872337  std: 63.984512
# i23 mean: 547.940063  std: 247.826172
# i24 mean: 9.021664  std: 4.555066
# i25 mean: 18.889168  std: 5.801007
# i26 mean: 44.288200  std: 16.301682
# i27 mean: 4.017601  std: 1.789919
# i28 mean: 0.021663  std: 0.295673

def doRegression():
    inputNodes = 12
    layer1Activations = ['hard_sigmoid']
    layer2Activations = ['relu','tanh','sigmoid','hard_sigmoid','linear']
    hiddenLayer1Nodes = 10
    hiddenLayer2Nodes = 0
    hiddenLayer3Nodes = 0
    outputLayerNodes = 1
    random.seed
    inFile = open(os.path.dirname(__file__)+'/../../forestfires12.txt', 'r')
    outFile = open('12resultDataHardSGM.txt','a')

    completeInputData  = []
    completeOutputData = []
    allRealOutputs     = []

    for line in inFile:
        inputData = line.split()
        output = float(inputData[-1].strip())
        inputData = map(float,inputData)

        #0-1 normalization from max values of field
        inputData[0]  = float(inputData[0] / 9)
        inputData[1]  = float(inputData[1] / 9)
        inputData[2]  = float(inputData[2])
        inputData[3]  = float(inputData[3])
        inputData[4] = float(inputData[4] / 96.20)
        inputData[5] = float(inputData[5] / 291.3)
        inputData[6] = float(inputData[6] / 860.6)
        inputData[7] = float(inputData[7] / 56.10)
        inputData[8] = float(inputData[8] / 33.30)
        inputData[9] = float(inputData[9] / 100)
        inputData[10] = float(inputData[10] / 9.40)
        inputData[11] = float(inputData[11] / 6.4)

        allRealOutputs.append(output)
        completeOutputData.append((np.log(1+output))/6.994)
        completeInputData.append(inputData[0:12])

    X = np.array(completeInputData,  "float32")
    Y = np.array(completeOutputData, "float32")

    totalRounds = 1

    for j in range(0,len(layer2Activations)):
        for numNodes in range(1, 200):
            numNodes += 1
            totalRMSE = 0
            for round in range(0,totalRounds):
                start_time = time.time()
                kf = cross_validation.KFold(n=len(completeInputData),n_folds=5,shuffle=True)
                skf = StratifiedKFold(Y,n_folds=5)
                totalPasses = 0
                foldRMSE = 0

                for train_index,test_index in kf:
                    totalPasses += 1
                    X_train, X_test = X[train_index], X[test_index]
                    Y_train, Y_test = Y[train_index], Y[test_index]

                    #####Start NN Section ######
                    model = Sequential()
                    sgd = SGD(lr=0.05,momentum=0.05)
                    model.add(Dense(inputNodes, numNodes))
                    model.add(Activation('hard_sigmoid'))
                    model.add(Dense(numNodes, outputLayerNodes))
                    model.add(Activation(layer2Activations[j]))
                    model.compile(loss='mse', optimizer=sgd)
                    model.fit(X_train, Y_train, nb_epoch=300,shuffle=1,verbose=0,batch_size=128)

                    predictions = (model.predict(X_test))

                    #reverse normalization
                    for i in range (0,len(predictions)):
                        predictions[i] *= 6.994
                        predictions[i] = math.exp(predictions[i])-1

                        Y_test[i] *= 6.994
                        Y_test[i] = math.exp(Y_test[i])-1

                        #print predictions[i]

                    mse = np.mean(np.square(predictions - Y_test))

                    score = np.sqrt(mse)
                    foldRMSE += score
                    # print "RMSE:"
                    # print score
                    # for i in range (0,len(predictions)):
                    #     print "%f           %f          "%(predictions[i],Y_test[i])

                # print "\nFolds: %i" % totalPasses
                # print "RMSE: %f\n\n" % float(foldRMSE/totalPasses)
                totalRMSE += float(foldRMSE/totalPasses)

            # print "\n Rounds: %i" % totalRounds
            print("RMSE: %f , inputNodes: %i, Layer 1 Act: hard_sigmoid,HL1 Nodes: %i, Layer 2 Act: %s, Time: %f\n" % (float(totalRMSE/totalRounds),inputNodes,numNodes,layer2Activations[j],time.time()-start_time))
            outFile.write("%f,%i,hard_sigmoid,%i,%s,%f\n" % (float(totalRMSE/totalRounds),inputNodes,numNodes,layer2Activations[j], time.time()-start_time))
    outFile.close()

def testFunction():
    print ""

if __name__=='__main__':
    # p1 = Process(target=doRegression("Thread-1",4))
    # p1.start()
    # p2 = Process(target=doRegression("Thread-2",6))
    # p2.start()
    # p3 = Process(target=doRegression("Thread-3",8))
    # p3.start()
    # p4 = Process(target=doRegression("Thread-4",10))
    # p4.start()
    # p5 = Process(target=doRegression("Thread-5",12))
    # p5.start()

    doRegression()
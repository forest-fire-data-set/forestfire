from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import containers
from keras.layers.core import Dense, AutoEncoder
from keras.activations import sigmoid
from keras.utils import np_utils
from keras.optimizers import SGD
import numpy as np
from sklearn import cross_validation

inFile = open('FFC3.txt', 'rU')
completeData   = []
allRealOutputs = []
sampleData = []
targetData = []

for line in inFile:
    inputData = line.split()
    output = float(inputData[-1].strip())
    inputData = map(float,inputData)

    #0-1 normalization from max values of field
    inputData[0]  = float(inputData[0] / 9)
    inputData[1]  = float(inputData[1] / 9)
    inputData[21] = float(inputData[21] / 96.20)
    inputData[22] = float(inputData[22] / 291.3)
    inputData[23] = float(inputData[23] / 860.6)
    inputData[24] = float(inputData[24] / 56.10)
    inputData[25] = float(inputData[25] / 33.30)
    inputData[26] = float(inputData[26] / 100)
    inputData[27] = float(inputData[27] / 9.40)
    inputData[28] = float(inputData[28] / 6.4)
    #inputData[-1] = output

    if output == 0:
        burnClass = [1,0,0]
    if output == 1:
        burnClass = [0,1,0]
    if output == 2:
        burnClass = [0,0,1]

    targetData.append((burnClass[0],burnClass[1],burnClass[2]))
    allRealOutputs.append(output)
    sampleData.append(inputData[0:29])

X = np.array(sampleData,  "float32")
Y = np.array(targetData, "float32")

batch_size = 64
nb_classes = 3
nb_epoch = 30
nb_hidden_layers = [29,600,500,400,300,400]

# # the data, shuffled and split between train and test sets
# (X_train, y_train), (X_test, y_test) = mnist.load_data()
# X_train = X_train.reshape(-1, 784)
# X_test = X_test.reshape(-1, 784)
# X_train = X_train.astype("float32") / 255.0
# X_test = X_test.astype("float32") / 255.0
# print(X_train.shape[0], 'train samples')
# print(X_test.shape[0], 'test samples')
#
# # convert class vectors to binary class matrices
# Y_train = np_utils.to_categorical(y_train, nb_classes)
# Y_test = np_utils.to_categorical(y_test, nb_classes)

kFolds = 2
kf = cross_validation.KFold(n=len(sampleData),n_folds=kFolds,shuffle=True)

for train_index,test_index in kf:
    X_train, X_test = X[train_index], X[test_index]
    Y_train, Y_test = Y[train_index], Y[test_index]
    Y_train = np_utils.to_categorical(Y_train,nb_classes)
    Y_test = np_utils.to_categorical(Y_test,nb_classes)
    # Layer-wise pretraining
    encoders = []
    X_train_tmp = np.copy(X_train)
    for i, (n_in, n_out) in enumerate(zip(nb_hidden_layers[:-1], nb_hidden_layers[1:]), start=1):
        print('Training the layer {}: Input {} -> Output {}'.format(i, n_in, n_out))
        # Create AE and training
        ae = Sequential()
        encoder = containers.Sequential([Dense(n_in, n_out, activation='tanh')])
        decoder = containers.Sequential([Dense(n_out, n_in, activation='tanh')])
        sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
        ae.add(AutoEncoder(encoder=encoder, decoder=decoder, output_reconstruction=False)) #, tie_weights=True))
        ae.compile(loss='mean_squared_error', optimizer=sgd)
        ae.fit(X_train_tmp, X_train_tmp, batch_size=batch_size, nb_epoch=nb_epoch)
        # Store trainined weight and update training data
        encoders.append(ae.layers[0].encoder)
        X_train_tmp = ae.predict(X_train_tmp)

    encoderWeights = []
    for encoder in encoders:
        encoderWeights.append(encoder.get_weights())

    # Fine-turning
    model = Sequential()
    indexEncoder = 0
    for encoder in encoders:
        model.add(encoder)
        encoder.set_weights(encoderWeights[indexEncoder])
        indexEncoder += 1
    model.add(Dense(nb_hidden_layers[-1],nb_classes, activation='relu'))

    model.compile(loss='categorical_crossentropy', optimizer='rmsprop')

    score = model.evaluate(X_test, Y_test, show_accuracy=True, verbose=0)
    print('Test score before fine turning:', score[0])
    print('Test accuracy before fine turning:', score[1])

    model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch,
              show_accuracy=True, validation_data=(X_test, Y_test))
    score = model.evaluate(X_test, Y_test, show_accuracy=True, verbose=0)
    print('Test score after fine turning:', score[0])
    print('Test accuracy after fine turning:', score[1])
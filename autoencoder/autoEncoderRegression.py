import os
import sklearn
from keras.models import Sequential
from keras.layers.core import Dense, Activation,AutoEncoder
from keras.layers.noise import GaussianNoise
from keras.optimizers import SGD,Adadelta
import numpy as np
import math
import random
import theano
from sklearn import cross_validation
from keras.layers import containers



#mean of output is: 12.8472920696
#std deviation is: 63.5942259828

# i0 mean: 4.669246  std: 2.311539
# i1 mean: 4.299807  std: 1.228710
# i21 mean: 90.644684  std: 5.514770
# i22 mean: 110.872337  std: 63.984512
# i23 mean: 547.940063  std: 247.826172
# i24 mean: 9.021664  std: 4.555066
# i25 mean: 18.889168  std: 5.801007
# i26 mean: 44.288200  std: 16.301682
# i27 mean: 4.017601  std: 1.789919
# i28 mean: 0.021663  std: 0.295673

if __name__=='__main__':
    random.seed
    inFile = open('forestfires.txt', 'r')
    completeInputData  = []
    completeOutputData = []
    allRealOutputs     = []

    for line in inFile:
        inputData = line.split()
        output = float(inputData[-1].strip())
        inputData = map(float,inputData)

        #0-1 normalization from max values of field
        inputData[0]  = float(inputData[0] / 9)
        inputData[1]  = float(inputData[1] / 9)
        inputData[21] = float(inputData[21] / 96.20)
        inputData[22] = float(inputData[22] / 291.3)
        inputData[23] = float(inputData[23] / 860.6)
        inputData[24] = float(inputData[24] / 56.10)
        inputData[25] = float(inputData[25] / 33.30)
        inputData[26] = float(inputData[26] / 100)
        inputData[27] = float(inputData[27] / 9.40)
        inputData[28] = float(inputData[28] / 6.4)

        allRealOutputs.append(output)
        completeOutputData.append((np.log(1+output))/6.994)
        completeInputData.append(inputData[0:29])

    X = np.array(completeInputData,  "float32")
    Y = np.array(completeOutputData, "float32")

    kf = cross_validation.KFold(n=len(completeInputData),n_folds=5,shuffle=True)

    ######Start NN Section ######
    totalPasses = 0
    foldRMSE = 0
    totalRMSE = 0
    for train_index,test_index in kf:
        totalPasses += 1
        adadelta = Adadelta()
        sgd = SGD(lr=0.01,momentum=0.9)
        X_train, X_test = X[train_index], X[test_index]
        Y_train, Y_test = Y[train_index], Y[test_index]

        encoder = containers.Sequential([Dense(29,20)])
        decoder = containers.Sequential([Dense(20,29)])

        autoencoder = AutoEncoder(encoder=encoder,decoder=decoder,output_reconstruction=True)

        model = Sequential()
        model.add(autoencoder)
        model.compile(optimizer=adadelta, loss='mse')
        model.fit(X_train,X_train,nb_epoch=10)

        autoencoder.output_reconstruction = False
        model.compile(optimizer=adadelta,loss='mse')
        representations = model.predict(X_test)

        # model.fit(X_test, representations, nb_epoch=1)

        autoencoder.output_reconstruction = True
        model.compile(optimizer=adadelta,loss='mse')
        model.fit(X_train,X_train, nb_epoch=15)


        # model.add(Dense(29, 10, init='normal'))
        # model.add(Activation('relu'))
        # model.add(Dense(10, 10, init='normal'))
        # model.add(Activation('relu'))
        # model.add(Dense(10, 10, init='normal'))
        # model.add(Activation('relu'))
        # model.add(Dense(10, 10, init='normal'))
        # model.add(Activation('relu'))
        # model.add(Dense(10, 10, init='normal'))
        # model.add(Activation('relu'))
        # model.add(Dense(10, 10, init='normal'))
        # model.add(Activation('relu'))
        # model.add(Dense(10, 10, init='normal'))
        # model.add(Activation('relu'))
        # model.add(Dense(10, 1, init='normal'))
        # model.add(Activation('relu'))
        #
        #
        # model.compile(loss='mse', optimizer=adadelta)
        # model.fit(X_train, Y_train, nb_epoch=50, shuffle=1, verbose=2, batch_size=64)
        #
        # predictions = (model.predict(X_test))
        # objective_score = model.evaluate(X_test,Y_test,batch_size=128)
        # print objective_score
        #
        # #reverse normalization
        # for i in range (0,len(predictions)):
        #     predictions[i] *= 6.994
        #     predictions[i] = math.exp(predictions[i])-1
        #
        #     Y_test[i] *= 6.994
        #     Y_test[i] = math.exp(Y_test[i])-1
        #
        #     #print predictions[i]
        #
        # mse = np.mean(np.square(predictions - Y_test))
        #
        # score = np.sqrt(mse)
        # foldRMSE += score

    totalRMSE += float(foldRMSE/totalPasses)
    print("RMSE: %f\n" % (float(totalRMSE)))